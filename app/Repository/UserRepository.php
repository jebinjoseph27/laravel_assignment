<?php


namespace App\Repository;


use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    public function updateProfile($data)
    {
        try {
            $update = User::where('id', $data['id'])->update($data);

            return $update;
        } catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function savePhoto($request)
    {
        try{
            $user_id = Auth::user()->id;
            if ($request->file('file') != null) {
                $filename = $request->file('file')->store('profile');
                $image = $filename;
                $update = User::where('id', $user_id)->update(['profile_photo' => $image]);
            }
            return true;
        }
        catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }

    public function updatePasswords($request)
    {
        try{
            Auth::user()->update([
                'password' => Hash::make($request['new_password'])
            ]);
            return true;
        }
        catch (Exception $e) {
            echo $e->getMessage();
            return false;
        }
    }
}
