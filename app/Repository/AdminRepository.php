<?php


namespace App\Repository;


use App\Admin;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminRepository
{


    public function checkEmail( $email = NULL )
    {

        if( !is_null( $email ) ) {
            try {

                return Admin::where( 'email', '=', $email )->first();

            } catch( Exception $e ) {
                echo $e->getMessage();
                return false;
            }
        }

    }


    public function isActiveUser( $email )
    {
        if( !is_null( $email ) ) {
            try {

                return Admin::where( 'email', '=', $email )->where( 'is_active', 1 )->first();
            } catch( Exception $e ) {
                echo $e->getMessage();
                return false;
            }
        }
    }

    public function listCustomerData($data){
        try
        {
            $customer = User::
                select('users.id',
                'users.name',
                'users.email',
                'users.date_of_birth',
                'users.mobile_no',
                'users.address',
                'users.profile_photo',
                'users.created_at'
//                                DB::raw("DATE_FORMAT(users.created_at, '%d %M %Y') as created_at")

                  );


            if ($data['search'] != '')
            {
                $customer->where('users.name', 'LIKE', '%'.$data['search'].'%');
                $customer->orWhere('users.email', 'LIKE', '%'.$data['search'].'%');
                $customer->orWhere('users.date_of_birth', 'LIKE', '%'.$data['search'].'%');
                $customer->orWhere('users.mobile_no', 'LIKE', '%'.$data['search'].'%');
                $customer->orWhere('users.address', 'LIKE', '%'.$data['search'].'%');
            }

            if($data['column'] == 0)
                $customer->orderBy('users.id',$data['order']);
            if($data['column'] == 1)
                $customer->orderBy('users.name',$data['order']);
            if($data['column'] == 2)
                $customer->orderBy('users.email',$data['order']);
            if($data['column'] == 3)
                $customer->orderBy('users.date_of_birth',$data['order']);
            if($data['column'] == 4)
                $customer->orderBy('users.address',$data['order']);

            if($data['column'] == 6)
                $customer->orderBy('users.created_at',$data['order']);


            $total  = User::
                select('users.*')
                ->count();

            return ['total' => $total, 'customer' => $customer ];
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
            return false;
        }
    }
    public function deleteCustomer($id){
        try
        {
            $result = User::destroy($id);

            return $result;
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
            return false;
        }
    }

    public function getCustomerDetail($id){
        try
        {
            $user = User::whereId($id)->first();
            return $user;
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
            return false;
        }
    }

    public function updateCustomer($info,$id){
        try
        {
            $user = User::findOrFail($id);
            $user->name = $info['name'];
            $user->date_of_birth = $info['date_of_birth'];
            $user->email = $info['email'];
            $user->address = $info['address'];
            $user->mobile_no = $info['mobile_no'];


            if (isset($info['password']))
                $user->password = $info['password'];
            if (isset($info['profile_photo']))
                $user->profile_photo = $info['profile_photo'];
            $user->save();


            return $user;
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
            return false;
        }
    }




}
