<?php

namespace App\Http\Controllers;

use App\Repository\UserRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->middleware('auth');
        $this->userRepository = $userRepository;

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function store(Request $request)
    {


        $rules = [
            'name' => 'required|max:254|regex:/^[a-zA-Z0-9 ~?"\'.,!@#$%^&*()[\]_+=;:\/\-]+$/',
            'date_of_birth' => 'required|date_format:Y-m-d',
            'address' => 'required|max:254|regex:/^[a-zA-Z0-9 ~?"\' .,!@#$%^&*()[\]_+=;:\/\-]+$/',
            'mobile_no' => 'required|numeric|digits:10',
        ];

        $customMessages = [
            'date_format' => 'The DOB does not match the format'
        ];

        $this->validate($request, $rules, $customMessages);


        $data = $request->only('name', 'date_of_birth', 'address', 'mobile');
        $data['id'] = Auth::user()->id;
        $data['name'] = $request->name;
        $data['date_of_birth'] = $request->date_of_birth;
        $data['address'] = $request->address;
        $data['mobile_no'] = $request->mobile_no;

        $profile = $this->userRepository->updateProfile($data);
        if ($profile)
            return redirect('home')->with('success', 'profile updated');
        else
            return redirect('home')->with('error', 'Technical error');

    }

    public function saveProfilePhoto(Request $request)
    {
        $rules = array(
            'file' => 'mimes:jpeg,jpg,png|required|max:10000',
        );

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $failureResponse = ['status' => 'failure', 'message' => $validator->errors()->getMessages()];
            return response()->Json($failureResponse);
        }
        $update = $this->userRepository->savePhoto($request);

        if (!$update) {
            $failureResponse = ['status' => 'failure', 'message' => 'Sorry! Technical error'];
            return response()->Json($failureResponse);
        }

        $successResponse = ['status' => 'success', 'message' => 'Profile photo updated successfully'];
        return response()->Json($successResponse);
    }

    public function changePassword()
    {
        return view('change-password');
    }
    public function changePasswordStore(Request $request)
    {
        $this->validate($request, [
            'current_password' => 'required',
            'new_password' => 'required|string|min:6',
            'password_confirmation' => 'required|string|same:new_password'
        ]);
        $check = Hash::check($request['current_password'], Auth::user()->password);
        $update = $this->userRepository->updatePasswords($request);

        if (!$check) {
            return redirect()->route('change-password')->with('error',  'Entered current password is wrong');
        }

        return redirect()->route('home')->with('success', 'Password has been updated successfully');
    }

}
