<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\AdminLoginRepository;
use App\Repository\AdminRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    private $adminRepository;

    public function __construct(AdminRepository $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }

    public function index()
    {
        return view('admin.admin-login');
    }


    public function login(Request $request)
    {
        $validatedData = \Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validatedData->fails()) {
            return response()->json(['status' => false, 'message' => $validatedData->messages(), 'error' => 0]);
        }

        if (!$this->adminRepository->checkEmail($request->email))
            return response()->json(['status' => false, 'message' => 'Invalid User name', 'error' => 1]);

        if (!$this->adminRepository->isActiveUser($request->email)) {

            return response()->json(['status' => false, 'message' => 'Inactive User', 'error' => 1]);

        }
        if (Auth::guard('admin_user')->attempt(['email' => $request->email, 'password' => $request->password])) {

            Auth::logoutOtherDevices($request->password);

            return response()->json(['status' => true, 'message' => 'User authenticated', 'error' => 0]);

        } else {
            return response()->json(['status' => false, 'message' => 'Invalid Password', 'error' => 1]);

        }

    }


    public function logout()
    {
        Auth::guard('admin_user')->logout();
        return redirect('admin-login');
    }

    public function dashboard()
    {
        return view('admin.dashboard');
    }
    public function customerList()
    {
        return view('admin.customer-list');
    }

    public function getCustomerList(Request $request){
        $data['search']  = $request->search['value'];
        $data['sort']     = $request->order;
        $data['column']   = $data['sort'][0]['column'];
        $data['order']   = $data['sort'][0]['dir'] == 'asc' ? "ASC" : "DESC" ;

        $listCustomer = $this->adminRepository->listCustomerData($data);
        $result['data']= $listCustomer['customer']->take($request->length)->skip($request->start)->get();

        $result['recordsTotal'] = $listCustomer['total'];
        $result['recordsFiltered'] =  $listCustomer['total'];
        echo json_encode($result);
    }

    public function deleteCustomer($id){
        $delete = $this->adminRepository->deleteCustomer($id);
        if(!$delete)
            return response()->Json(['status' => false , 'message' => 'Sorry! Technical error']);
        return response()->Json(['status' => true , 'message' => 'Customer deleted. This page will redirect in 3 seconds']);
    }

    public function editCustomer($id){

        $customerDetail = $this->adminRepository->getCustomerDetail($id);

        return view('admin.customer-edit',compact('customerDetail'));
    }

    public function updateCustomer(Request $request){
        $rule = [
            'name' => 'required|max:254|regex:/^[a-zA-Z0-9 ~?"\'.,!@#$%^&*()[\]_+=;:\/\-]+$/',
            'date_of_birth' => 'required|date_format:Y-m-d',
            'email' => 'required|email|unique:users,email,'.$request->post('id'),
            'address' => 'required|max:254|regex:/^[a-zA-Z0-9 ~?"\' .,!@#$%^&*()[\]_+=;:\/\-]+$/',
            'mobile_no' => 'required|numeric|digits:10|unique:users,mobile_no,'.$request->post('id'),

        ];
        if($request->file('imgprofile') != null){
            $rule['imgprofile'] = 'required|image|mimes:jpeg,png,jpg|max:10000';
        }
        if($request->password != "null" && $request->password != "" ){
            $rule['password'] = 'required|min:8|required_with:password_confirmation|same:password_confirmation';
            $rule['password_confirmation'] = 'required';
        }
        $validatedData = \Validator::make($request->all(), $rule);

        if ($validatedData->fails()) {
            return response()->json(['status'=>false,'message' => $validatedData->messages(), 200]);
        }

        $id =  $request->post('id');

        $data = array(
            'name' => $request->post('name'),
            'date_of_birth'    => $request->post('date_of_birth'),
            'email'     =>  $request->post('email'),
            'address'      =>  $request->post('address'),
            'mobile_no'   =>  $request->post('mobile_no'),
        );
        if($request->file('imgprofile') != null){
            $filename = $request->file('file')->store('profile');
            $data['profile_photo'] = $filename;
        }
        if($request->password != "null" && $request->password != "" ){
            $data['password'] =   Hash::make($request->post('password'));
        }
// dd($data);
        $update = $this->adminRepository->updateCustomer($data,$id);

        if(!$update)
            return response()->Json(['status' => false , 'message' => 'Sorry! Technical error']);
        return response()->json(['status'=>true,'message' => 'customer Account successfully updated.']);
    }

}
