<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/profile', 'HomeController@store')->name('store');
Route::post('/save-profile-photo', 'HomeController@saveProfilePhoto');
Route::get('/change-password', 'HomeController@changePassword')->name('change-password');
Route::post('/change-password', 'HomeController@changePasswordStore')->name('change-password');

//----------Admin Login------------------
Route::get('admin-login', 'Admin\AdminController@index');
Route::post('admin/sign-in', 'Admin\AdminController@login');
Route::group(["middleware" => "admin_user", 'prefix' => 'admin'], function () {
    Route::get('/dashboard', 'Admin\AdminController@dashboard')->name('admin-dashboard');
    Route::get('/customer-list', 'Admin\AdminController@customerList')->name('customer-list');
    Route::get('/get-customer-list', 'Admin\AdminController@getCustomerList');
    Route::get('/delete-customer/{id}', 'Admin\AdminController@deleteCustomer');
    Route::get('/edit-customer/{id}', 'Admin\AdminController@editCustomer');
    Route::post('/update-customer/', 'Admin\AdminController@updateCustomer');

});
