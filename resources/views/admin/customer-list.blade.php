@extends('admin.layouts.app-layout')

@section('content')

    @include('admin.layouts.admin-login-header')
    @include('admin.layouts.sidebar')
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">
                    <h1 class="mt-4">Customer List</h1>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item"><a href="{{ route('admin-dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Customer list</li>
                    </ol>

                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-table mr-1"></i>
                            DataTable
                        </div>
                        <div class="table-responsive">
                            <table id="customer_datatable" class="display table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Sl. No.</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Date of Birth</th>
                                    <th>address</th>
                                    <th>image</th>
                                    <th>Created At</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody id="adminuser_tbody">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </main>

        </div>

@endsection
@push('js')
    <script src={{\Illuminate\Support\Facades\URL::asset('/public/js/admin/customer.js')}}></script>

    @endpush
