@extends('admin.layouts.app-layout')

@section('content')

@include('admin.layouts.admin-login-header')
@include('admin.layouts.sidebar')
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Dashboard</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>

        </div>
    </main>

</div>
@endsection
