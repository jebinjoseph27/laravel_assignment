<!DOCTYPE html>
<html lang="en">


@include('admin.layouts.header')
<input type="hidden" id="siteurl" value="{{url('')}}">


<body class="bg-primary">

@yield('content')

</body>
@include('admin.layouts.footer')
@stack('js')
</html>
