@extends('admin.layouts.app-layout')

@section('content')

    @include('admin.layouts.admin-login-header')
    @include('admin.layouts.sidebar')
    <title>Edit Customer</title>
    <div id="layoutSidenav_content">
        <!-- ============ Body content start ============= -->
        <div class="main-content-wrap sidenav-open d-flex flex-column">
            <div class="breadcrumb">
                <h1>Edit Customer</h1>
            </div>

            <div class="separator-breadcrumb border-top"></div>
            <div class="col-md-12 alert-box">
                <div class="alert alert-card d-none" role="alert">
                    <strong class="text-capitalize type"></strong><span class="success-alert"></span>.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title mb-3"></div>
                            <form id="update_customer" method="post"  enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row">
                                    <input type="hidden" name="id" value="{{ $customerDetail['id'] }}">

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="firstName1">Name</label>
                                        <input type="text" class="form-control" name="name" id="name" placeholder="Enter your full name" value="{{ $customerDetail['name']}}">
                                        <div class="invalid-feedback error error_full_name ">dfgdfgsdasdassd</div>
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="firstName1">Date of birth</label>
                                        <input type="date" class="form-control" name="date_of_birth" id="dob" placeholder="DOB" value="{{ $customerDetail['date_of_birth']}}">
                                        <div class="invalid-feedback error error_full_name "></div>
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="lastName1">Email</label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" value="{{ $customerDetail['email']}}">
                                        <div class="invalid-feedback error error_email "></div>
                                    </div>


                                    <div class="col-md-6 form-group mb-3">
                                        <label for="phone">Mobile</label>
                                        <input class="form-control" id="mobile" type="number" name="mobile_no" placeholder="Enter mobile"  value="{{ $customerDetail['mobile_no']}}">
                                        <div class="invalid-feedback error error_mobile_no "></div>
                                    </div>


                                    <div class="col-md-6 form-group mb-3">
                                        <label for="exampleInputEmail1">Password (leave blank if you don't want to change it) :</label>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" value="">
                                        <div class="invalid-feedback error error_password "></div>
                                        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="exampleInputEmail1">Confirm Password</label>
                                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Enter Password">
                                        <div class="invalid-feedback error error_password_confirmation "></div>
                                        <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                                    </div>


                                    <div class="col-md-6 form-group mb-3">
                                        <label for="phone">Address</label>
                                        <input class="form-control" id="address" type="text" name="address" placeholder="Enter address" value="{{ $customerDetail['address'] }}">
                                        <div class="invalid-feedback error error_address "></div>
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="phone">Profile Image</label>
                                        <div class="input-group mb-3">
                                            <div class="custom-file">
                                                <input type="file" class="form-control" name="imgprofile" id="imgprofile">
                                                <!-- <label class="custom-file-label" for="imgprofile" aria-describedby="imgprofile">Choose Image</label> -->
                                            </div>

                                            <div class="invalid-feedback error error_imgprofile "></div>
                                        </div>
                                        @if($customerDetail['profile_photo'] )
                                            <div class=" image-thumb text-white o-hidden mb-3">
                                                <img src="{{ url('storage/app') }}/{{ $customerDetail['profile_photo']}}" style="width: 100px;height: 100px" alt="">
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>



            </div>



        </div>


    </div>
@endsection
@push('js')
    <script src={{\Illuminate\Support\Facades\URL::asset('/public/js/admin/customer.js')}}></script>

@endpush
