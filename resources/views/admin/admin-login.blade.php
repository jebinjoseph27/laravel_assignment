@extends('admin.layouts.app-layout')

@section('content')
    <title>Admins-login</title>
    <div id="layoutAuthentication">
    <div id="layoutAuthentication_content">
        <main>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <div class="card shadow-lg border-0 rounded-lg mt-5">
                            <div class="card-header"><h3 class="text-center font-weight-light my-4">Login</h3></div>
                            <div class="card-body">
                                <div class="col-md-12 alert-box">
                                    <div class="alert alert-card d-none" role="alert">
                                        <strong class="text-capitalize type"></strong><span class="success-alert"></span>.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">x</span>
                                        </button>
                                    </div>
                                </div>
                                <form  id="admin-login">
                                    @csrf
                                    <div class="form-group">
                                        <label class="small mb-1" for="inputEmailAddress">Email</label>
                                        <input class="form-control py-4" id="email" name="email" type="email" placeholder="Enter email address" />
                                         <div class="invalid-feedback error error_email"></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="small mb-1" for="inputPassword">Password</label>
                                        <input class="form-control py-4" id="password" name="password"type="password" placeholder="Enter password" />
                                         <div class="invalid-feedback error error_password"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" id="rememberPasswordCheck" type="checkbox" />
                                            <label class="custom-control-label" for="rememberPasswordCheck">Remember password</label>
                                        </div>
                                    </div>
                                    <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
{{--                                        <a class="small" href="password.html">Forgot Password?</a>--}}
                                        <button type="submit" class="btn btn-primary">Login</button>
                                    </div>
                                </form>
                            </div>
{{--                            <div class="card-footer text-center">--}}
{{--                                <div class="small"><a href="register.html">Need an account? Sign up!</a></div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>

</div>

    @endsection
@push('js')
    <script>
    $( document ).ready(function() {
        var url = $('#siteurl').val();

    $('#admin-login').validate({
        rules: {
            email: {
                required: true,
                email: true,
            },
            password: {
                required: true,
                minlength: 6
            },
        },

        ignore: ' ',
        submitHandler: function(form) {

            var form_data = new FormData($('#admin-login')[0]);
            $(".alert-box .type").html('Processing...');
            $(".alert-box .alert").addClass('alert-primary').removeClass('d-none');

            $.ajax({
                type: "POST",
                url: url + "/admin/sign-in",
                dataType: 'JSON',
                data : form_data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(result){

                    if(result.status == 1){

                        window.location.href = url+"/admin/dashboard";
                    }
                    else
                    {
                        if(result.message){
                            if(result.error){
                                $(".alert-box .type").html('Sorry! ');
                                $(".success-alert").html(result.message);
                                $(".alert-box .alert").addClass('alert-danger').removeClass('d-none');
                                $('alert').delay(3000).addClass('d-none');
                            }else{
                                $.each( result.message, function( key, value ) {
                                    if(value[0])
                                        $("#admin-login .error_"+key).html(value[0]);
                                });
                            }
                        }
                    }

                },
                error: function(response){
                   alert("technical error please contact your technical team")
                }
            })

        }
    });
        console.log( "ready!" );
    });
</script>
@endpush
