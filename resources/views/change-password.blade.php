@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <legend>Change Password</legend>
            </div>
        </div>
        <div class="row">

            <div class="col-sm-6 col-sm-offset-3">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                <p class="text-center"></p>
                <form method="post" id="passwordForm">
                    @csrf
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Current Password</label>
                        </div>
                    </div>
                    <input
                        type="password"
                        class="form-control form-input{{ $errors->has('current_password') ? ' is-invalid' : '' }}"
                        id="current_password"
                        name="current_password"
                        value="{{ old('current_password') }}"
                        required>
                    @if ($errors->has('current_password'))
                        <span class="invalid-feedback">
                                                    <strong
                                                        class="is-invalid">{{ $errors->first('current_password') }}</strong>
                                                </span>
                    @endif
                    <div class="row">
                        <div class="col-sm-6">
                            <label>New Password</label>
                        </div>
                    </div>
                    <input
                        id="new_password"
                        type="password"
                        class="form-input form-control{{ $errors->has('new_password') ? ' is-invalid' : '' }}"
                        name="new_password"
                        required
                        onselectstart="return false" onpaste="return false;" onCopy="return false" onCut="return false"
                        onDrag="return false" onDrop="return false" autocomplete=off>

                    @if ($errors->has('new_password'))
                        <span class="invalid-feedback">
                                                    <strong
                                                        class="is-invalid">{{ $errors->first('new_password') }}</strong>
                                                </span>
                    @endif
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Confirm Password</label>
                        </div>
                    </div>
                    <input
                        id="password-confirm"
                        type="password"
                        class="form-input form-control"
                        name="password_confirmation"
                        required
                        onselectstart="return false" onpaste="return false;" onCopy="return false" onCut="return false"
                        onDrag="return false" onDrop="return false" autocomplete=off
                        equalTo="#new_password">

                    <div class="row">
                        <div class="col-sm-12">
                            <label></label>
                        </div>
                    </div>
                    <div class="form-group regbtn">
                        <a class="col-xs-6 btn btn-primary btn-load btn-lg" href="{{ route('home') }}">CANCEL</a>
                        <input class="col-xs-6 btn btn-primary btn-load btn-lg" type="submit" class="red_btn"
                               value="SAVE"/>
                    </div>

                </form>
            </div><!--/col-sm-6-->
        </div><!--/row-->
    </div>






@endsection
@push('js')

@endpush
