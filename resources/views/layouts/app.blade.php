<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

@include('layouts.css')
@stack('css')



    @include('layouts.header')
    <input type="hidden" id="siteurl" value="{{url('')}}">

</head>
<body>
    <div id="app">

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
@include('layouts.footer')
@stack('js')
</html>
