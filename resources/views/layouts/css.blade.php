<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5e97f076931c3700123db8fd&product=inline-share-buttons&cms=sop' async='async'></script>

<link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800&display=swap" rel="stylesheet">
<link rel="stylesheet" href="{{url('public/css/style.css')}}">
<link rel="stylesheet" href="{{url('resources/css/front/common.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">





<!-- Styles -->
<link href="{{ asset('public/css/app.css') }}" rel="stylesheet">

<title>User profile form requirement</title>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha256-3dkvEK0WLHRJ7/Csr0BZjAWxERc5WH7bdeUya2aXxdU= sha512-+L4yy6FRcDGbXJ9mPG8MT/3UCDzwR9gPeyFNMCtInsol++5m3bk2bXWKdZjvybmohrAsn3Ua5x8gfLnbE1YkOg==" crossorigin="anonymous">
<!-- Bootstrap Core CSS -->
<!--     <link href="css/bootstrap.min.css" rel="stylesheet"> -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">

<!-- Custom CSS -->
<style>


    .othertop{margin-top:10px;}

     #custom-file-label::after {
         position: absolute;
         right: -11px;
         bottom: 34px;
         z-index: 3;
         display: block;
         height: calc(1.5em + 1.034rem);
         padding: 0.517rem 1rem;
         line-height: 1.5;
         color: #1D1D1D;
         content: "Browse";
         background-color: #EDEFF4;
         border-left: inherit;
     }


</style>
<!-- Scripts -->
<script src="{{ asset('public/js/app.js') }}" defer></script>

<!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">



