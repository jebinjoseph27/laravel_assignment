@extends('layouts.app')

@section('content')


    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif

        <div class="row">
            <div class="col-md-10 ">
                <form class="form-horizontal" method="post" action="{{ route('store') }}">
                    @csrf

                    <fieldset>

                        <!-- Form Name -->
                        <legend>User profile </legend>

                        <!-- Text input-->

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="Name (Full name)">Name (Full name)*</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user">
                                        </i>
                                    </div>
                                    <input
                                        type="text"
                                        class="form-control form-input input-md{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                        id="name"
                                        name="name"
                                        value="{{ old('name', Auth::user()->name ? Auth::user()->name : '') }}"
                                        required>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                                    <strong class="is-invalid">{{ $errors->first('name') }}</strong>
                                                </span>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="Date Of Birth">Date Of Birth*</label>
                            <div class="col-md-4">

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-birthday-cake"></i>

                                    </div>
                                    <input type="date"
                                           class="form-control form-input input-md{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}"
                                           id="date_of_birth"
                                           name="date_of_birth"
                                           value="{{ old('date_of_birth', Auth::user()->date_of_birth ? Auth::user()->date_of_birth : '') }}"
                                           required>
                                    @if ($errors->has('date_of_birth'))
                                        <span class="invalid-feedback">
                                                    <strong
                                                        class="is-invalid">{{ $errors->first('date_of_birth') }}</strong>
                                                </span>
                                    @endif
                                </div>


                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label col-xs-12" for="Permanent Address">Permanent
                                Address*</label>
                            <div class="col-md-4">
                                <textarea
                                    class="form-control form-input input-md{{ $errors->has('address') ? ' is-invalid' : '' }}"
                                    id="address"
                                    name="address"
                                    required>
                                    {{ old('address', Auth::user()->address ? Auth::user()->address : '') }}
                                </textarea>
                                @if ($errors->has('address'))
                                    <span class="invalid-feedback">
                                                    <strong class="is-invalid">{{ $errors->first('address') }}</strong>
                                                </span>
                                @endif
                            </div>

                        </div>


                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="Phone number ">Phone number *</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-mobile fa-1x" style="font-size: 20px;"></i>

                                    </div>
                                    <input
                                        type="number"
                                        class="form-control form-input{{ $errors->has('mobile_no') ? ' is-invalid' : '' }}"
                                        id="mobile_no"
                                        name="mobile_no"
                                        value="{{ old('mobile_no', Auth::user()->mobile_no ? Auth::user()->mobile_no : '') }}"
                                        required>
                                    @if ($errors->has('mobile_no'))
                                        <span class="invalid-feedback">
                                                    <strong
                                                        class="is-invalid">{{ $errors->first('mobile_no') }}</strong>
                                                </span>
                                    @endif
                                </div>


                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="Email Address">Email Address</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-envelope-o"></i>
                                    </div>
                                    <input id="email_address" name="email_address" type="text" disabled
                                           value="{{Auth::user()->email}}"
                                           placeholder="email_address" class="form-control input-md">

                                </div>

                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-success"><span
                                        class="glyphicon glyphicon-thumbs-up"></span>
                                    Submit
                                </button>
{{--                                <a href="#" class="btn btn-danger" value=""><span--}}
{{--                                        class="glyphicon glyphicon-remove-sign"></span> Clear</a>--}}

                            </div>
                        </div>

                    </fieldset>
                </form>
            </div>


            <div class="col-md-2 hidden-xs">
                <form action="">
                    <img src="{{Auth::user()->profile_photo?url('/') . "/storage/app/" .Auth::user()->profile_photo:url("/")."/public/images/No-image-found.jpg"}}"
                         class="img-responsive img-thumbnail ">
                    <input type="file" class="custom-file-input" id="js_profile_img" onchange="readURL(this);">
                    <label class="align-items-center custom-file-label d-flex justify-content-center border-0"
                           for="era_profile_img" data-browse="Change">
                    </label>
                </form>

            </div>
        </div>
    </div>






@endsection
@push('js')
    <script >
        function readURL(input) {

            var form_data = new FormData();
            var file_data = $('#js_profile_img').prop('files')[0];
            form_data.append('file', file_data);
            var imagefile = file_data.type;
            var match = ["image/jpeg", "image/png", "image/jpg"];
            if (((imagefile === match[0]) || (imagefile === match[1]) || (imagefile === match[2]))) {

                $.ajax({
                    url: $('#siteurl').val() + "/save-profile-photo",
                    type: 'POST',
                    dataType: 'JSON',
                    data: form_data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (result) {

                        if (result.status == 'success') {

                            var elmt = $(input);
                            if (input.files && input.files[0]) {
                                var reader = new FileReader();
                                reader.onload = function (e) {
                                    $(elmt.parent().find('img')).attr('src', e.target.result);
                                };
                                reader.readAsDataURL(input.files[0]);
                            }

                        } else {
                            alert(result.message)
                            setTimeout(function () {
                                location.reload();
                            }, 3000);

                        }
                    }
                });
            } else {
                $('#js_profile_img').val('');
                alert('You have selected an invalid image, please try to upload jpg or png image!', 'danger');
            }


        }
    </script>
@endpush
