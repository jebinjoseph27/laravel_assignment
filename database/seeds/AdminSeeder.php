<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement ('SET FOREIGN_KEY_CHECKS=0;');
        DB::table ('admin')->truncate();

        DB::statement ('SET FOREIGN_KEY_CHECKS=1;');
        $data = array(
            'email' => 'admin@admin.com',
            'password' => Hash::make('password'),
            'name' => 'demo Project Super admin',
            'is_active' => 1,
        );
        DB::table ('admin')->insert ($data);
    }
}
