<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUserTableaddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->date('date_of_birth')->nullable()->after('email');
            $table->string('mobile_no',11)->unique()->nullable()->after('date_of_birth');
            $table->string('address',255)->nullable()->after('mobile_no');
            $table->string('profile_photo',255)->nullable()->after('address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'is_closed')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn(['date_of_birth']);
                $table->dropColumn(['mobile_no']);
                $table->dropColumn(['address']);
                $table->dropColumn(['profile_photo']);
            });
        }
    }
}
