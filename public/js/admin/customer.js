$(document).ready(function () {

    var url = $('#siteurl').val();


    if($('#customer_datatable').length > 0)
    {
        $('#customer_datatable').DataTable({
            processing: true,
            serverSide: true,
            lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
            dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            ajax: url+'/admin/get-customer-list',
            columns: [
                {
                    data: "null",
                    width: '50',
                    render : function(data,type,full) {
                        return full.id;
                    }
                },
                {
                    data: "null",
                    width: '200',
                    render : function(data,type,full) {
                        return full.name;
                    }
                },

                {
                    data: "null",
                    width: '300',
                    render : function(data,type,full) {
                        return full.email;
                    }
                },
                {
                    data: "null",
                    width: '300',
                    render : function(data,type,full) {
                        return full.mobile_no;
                    }
                },
                {
                    data: "null",
                    width: '300',
                    render : function(data,type,full) {
                        return full.date_of_birth;
                    }
                },
                {
                    data: "null",
                    width: '300',
                    render : function(data,type,full) {
                        return full.address;
                    }
                },
                {
                    data: "null",
                    width: '300',
                    bSortable: false,
                    render : function(data,type,full) {
                        return full.profile_photo?'<img style="height:100px; width: 100px;" src="'+url+"/storage/app/"+full.profile_photo+'">':'<img style="height:100px; width: 100px;" src="'+url+'/public/images/No-image-found.jpg">'
                    }
                },
                {
                    data: "null",
                    width: '300',
                    bSortable: false,
                    render : function(data,type,full) {
                        return full.created_at;
                    }
                },

                {
                    data: "null",
                    width: '400',
                    bSortable: false,
                    render : function(data,type,full) {
                        return '<a href="'+url+'/admin/edit-customer/'+full.id+'" class="btn btn-primary btn-rounded m-1"title="Edit">Edit<i style="font-size: 1.3rem;" class="i-File-Edit"></i></a> &nbsp;&nbsp; ' +
                            ' <a  href="javascript:void(0)" data-id="'+full.id+'" " class="btn btn-danger btn-rounded m-1 delete_customer" title="Delete">Delete<i style="font-size: 1.3rem;" class="i-Delete-File"></i></a>';
                    }
                },


            ],
        });
    }

    $(document).on("click",".delete_customer",function(e){
        e.preventDefault();

        var customer_id = $(this).attr('data-id');

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0CC27E',
            cancelButtonColor: '#FF586B',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success mr-5',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function () {
            $.ajax({
                url: url + '/admin/delete-customer/'+customer_id,
                type: 'GET',
                dataType: 'json',
                success: function (data) {

                    if(data.status)
                    {
                        swal(
                            'Deleted!',
                            data.message,
                            'success'
                        ),
                            setTimeout(function(){
                                location.reload();
                            }, 3000);
                    }
                    else{
                        swal(
                            'No Deleted!',
                            data.message,
                            'failure'
                        );

                    }
                },
                error: function (message) {

                }
            });


        }, function (dismiss) {

            if (dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'Your file is safe :)',
                    'error'
                )
            }

        })

    });






    $('#update_customer').on('submit',function (e) {
        e.preventDefault();
        var file_data = $('#imgprofile').prop('files')[0];
        var form_data = new FormData($('#update_customer')[0]);
        form_data.append('file', file_data);

        $.ajax({
            type: "POST",
            url: url + "/admin/update-customer",
            dataType: 'JSON',
            data : form_data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
                $(".error").html('');
            },
            success: function(result){
                if(result.status == 1){
                    $(".alert-box .alert").addClass(' alert-success').removeClass('d-none');
                    $(".alert-box .type").html('Success! ');
                    $(".success-alert").html(result.message);
                    setTimeout(function(){
                        window.location.href = url+"/admin/customer-list";
                    }, 1500);
                }else{
                    if(result.message){
                        if(result.error){
                            $(".alert-box .type").html('Sorry! ');
                            $(".success-alert").html(result.message);
                            $(".alert-box .alert").addClass('alert-danger').removeClass('d-none');
                            $('alert').delay(3000).addClass('d-none');
                        }else{
                            $.each( result.message, function( key, value ) {
                                if(value[0])
                                    $("#update_customer .error_"+key).html(value[0]);
                            });
                            $('.invalid-feedback').css('display','block');

                        }
                    }
                }


            }
        });

    });

});

